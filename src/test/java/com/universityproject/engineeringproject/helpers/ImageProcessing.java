package com.universityproject.engineeringproject.helpers;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImageProcessing {

    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    static final String defaultResultPath = "src/test/resources/imgTemplates/result.png";

    // @returns true when feature is detected approx. at the same place
    //    by 3 methods separately
    public static boolean isFeatureOnTheScrrenshot(File src, String templatePath){
        Double[] res1 = findFeatureOnTheScrrenshot(src.getPath(), templatePath, Imgproc.TM_SQDIFF_NORMED);
        Double[] res2 = findFeatureOnTheScrrenshot(src.getPath(), templatePath, Imgproc.TM_CCOEFF_NORMED);
        Double[] res3 = findFeatureOnTheScrrenshot(src.getPath(), templatePath, Imgproc.TM_CCORR_NORMED);
        if(Math.abs(res1[0]-res2[0])<=(res1[0]*0.1)
            && Math.abs(res1[1]-res2[1])<=(res1[1]*0.1)
            && Math.abs(res2[0]-res3[0])<=(res2[0]*0.1)
            && Math.abs(res2[1]-res3[1])<=(res2[1]*0.1)
            && Math.abs(res1[0]-res3[0])<=(res1[0]*0.1)
            && Math.abs(res1[1]-res3[1])<=(res1[1]*0.1)){
            return true;
        }
        else{   return false;   }
    }

    //@returns top left point of found feature using best matching method and visualizes result
    public static Double[] findFeatureOnTheScrrenshot(File sourceFile, String templatePath){
        Double[] startLocation= findFeatureOnTheScrrenshot(sourceFile.getPath(), templatePath, Imgproc.TM_CCOEFF);
        drawResultOnSourceImg(startLocation, sourceFile.getPath(), templatePath, defaultResultPath);

        return startLocation;
    }

    //Returns top left point of found feature with given method
    public static Double[] findFeatureOnTheScrrenshot(String sourcePath, String templatePath, int method){
        //read template and source image(screenshot)
        Mat template = Imgcodecs.imread(templatePath);
        Mat source = Imgcodecs.imread(sourcePath);
        Mat output = new Mat();

        //find template on source
        Imgproc.matchTemplate(source, template, output, method);
        //localize best match
        Core.normalize(output, output,1,0,Core.NORM_MINMAX);
        MinMaxLocResult minMaxLocResult = Core.minMaxLoc(output);
        Point location;
        if(method==Imgproc.TM_SQDIFF ||
            method==Imgproc.TM_SQDIFF_NORMED){
            location = minMaxLocResult.minLoc;
        } else {
            location = minMaxLocResult.maxLoc;
        }
        //Logger.getLogger("logger").log(Level.INFO, "Location: "+location.x+location.y);

        return new Double[] {location.x, location.y};
    }

    //Visualizes result of feature matching
    public static void drawResultOnSourceImg(Double[] location, String sourcePath, String templatePath, String resPath){
        Mat source = Imgcodecs.imread(sourcePath);
        Mat template = Imgcodecs.imread(templatePath);
        Imgproc.rectangle(source,
                new Point(location[0], location[1]),
                new Point(location[0] + template.cols(), location[1] + template.rows()),
                new Scalar(0, 255, 0));
        Imgcodecs.imwrite(resPath, source);
        Logger.getLogger("logger").log(Level.INFO, "Result of feature search saved for reference");
    }

    public static Double[] getCentreCoordinates(Double[] startLocation, String imagePath){
        Mat img = Imgcodecs.imread(imagePath);
        return new Double[] {startLocation[0] + img.cols()/2,
                            startLocation[1] + img.rows()/2};
    }
}
