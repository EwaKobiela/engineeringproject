package com.universityproject.engineeringproject.helpers;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SeleniumUtility {

    static WebDriver driver = new ChromeDriver();
    private static final String defaultResultPath = "src/test/resources/imgTemplates/src.png";
    private static final double corrFactor = 0.8;

    public static void waitForElementVisible(By selector){
        WebDriverWait waitForElement = new WebDriverWait(driver, 20);
        waitForElement.until(ExpectedConditions.visibilityOfElementLocated(selector));
        Logger.getLogger("logger").log(Level.INFO, "Element ["+selector+"] was found.");
    }

    public static WebElement findElement(By selector){
        return driver.findElement(selector);
    }

    public static WebElement findElementQuietly(By selector){
        WebElement element = null;
        try{
            element = driver.findElement(selector);
        } catch (NoSuchElementException e){
            Logger.getLogger("logger").log(Level.INFO, "Element ["+selector+"] was not found.");
        }
        return element;
    }

    public static boolean isElementVisible(By selector){
        if(findElementQuietly(selector) != null){
            return true;
        } else {
            return false;
        }
    }

    public static void moveToElement(WebElement element){
        Actions move = new Actions(driver);
        move.moveToElement(element);
        move.perform();
    }

    public static void clickElement(By selector){
        clickElement(selector, true, 20);
    }

    public static void clickElement(By selector, Boolean scroll, int time){
        if(scroll){
            moveToElement(driver.findElement(selector));
        }
        new WebDriverWait(driver, 20)
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(selector))
                .click();
    }

    public static void openPage(String url){
        driver.get(url);
    }

    public static void setWindowSize(int width, int height){
        driver.manage().window().setSize(new Dimension(width, height));
    }

    public static void maximizeScreen(){
        driver.manage().window().maximize();
    }

    public static File takeScreenshot() throws Exception{
        TimeUnit.SECONDS.sleep(10);     //wait for game to be loaded
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(defaultResultPath));
        Logger.getLogger("logger").log(Level.INFO, "Screenshot was saved.");
        return scrFile;
    }

    public static void clickAtCoordinates(By selector, Double[] coordinates) throws AWTException {
        Point iFramePosiiton = driver.findElement(selector).getLocation();
        Robot bot = new Robot();
        System.out.print("Input position: "+iFramePosiiton.getX()+coordinates[0]+" "+iFramePosiiton.getY()+coordinates[1]);
        Double xVal = (iFramePosiiton.getX()+coordinates[0])*corrFactor;
        Double yVal = (iFramePosiiton.getY()+coordinates[1] + 120)*corrFactor;
        bot.mouseMove(xVal.intValue(), yVal.intValue());
        System.out.print("Mouse position: "+xVal+" "+yVal);
        bot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }

}
