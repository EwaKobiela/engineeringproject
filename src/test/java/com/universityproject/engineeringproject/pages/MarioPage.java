package com.universityproject.engineeringproject.pages;

import com.universityproject.engineeringproject.helpers.ImageProcessing;
import com.universityproject.engineeringproject.helpers.SeleniumUtility;
import org.openqa.selenium.By;

import java.awt.*;
import java.io.File;

public class MarioPage {

    private String baseURL = "https://cdn.kiz10.com/upload/games/htmlgames/super-mario-run-online-2017/1496134415/index.html";

    //selectors
    private static final By selectorAgreeToCookiesButton = By.className("fc-cta-consent");
    private static final By selectorPlayButton = By.id("show-embed");
    private static final By selectorGameiFrame = By.id("imaContainer");

    //paths to image templates
    private static final String playButtonTemplatePath = "src/test/resources/img/playButton.png";
    private static final String marioStartPageImagePath = "src/test/resources/img/src.png";

    public MarioPage openMarioPage(){
        SeleniumUtility.openPage(baseURL);
        return this;
    }

    public void maximizeScreen(){
        SeleniumUtility.maximizeScreen();
    }

    public File getScreenshot() throws Exception {
        return SeleniumUtility.takeScreenshot();
    }

    public boolean isPlayButtonVisible(File srcFile){
        return ImageProcessing.isFeatureOnTheScrrenshot(srcFile, playButtonTemplatePath);
    }

    public MarioPage clickPlayButton(File srcFile) throws AWTException {
        Double[] coordinates = ImageProcessing
                .getCentreCoordinates(ImageProcessing
                        .findFeatureOnTheScrrenshot(srcFile, playButtonTemplatePath), playButtonTemplatePath);
        SeleniumUtility.clickAtCoordinates(selectorGameiFrame, coordinates);
        return this;
    }

}
