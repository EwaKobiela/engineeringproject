package com.universityproject.engineeringproject.tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import com.universityproject.engineeringproject.pages.MarioPage;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MarioPageTest {

    static MarioPage marioPage;

    @BeforeAll
    public static void setUpAll() {
        System.setProperty("webdriver.chrome.driver","src/test/resources/driver/chromedriver.exe");
        SelenideLogger.addListener("allure", new AllureSelenide());
        marioPage = new MarioPage();
    }

    @Test
    public void checkIfStartButtonIsVisible() throws Exception {
        marioPage.maximizeScreen();
        marioPage.openMarioPage();
        assertThat(marioPage.isPlayButtonVisible(marioPage.getScreenshot())).isTrue();
    }

    @Test
    public void findAndClickStartButton() throws Exception {
        marioPage.maximizeScreen();
        marioPage.openMarioPage();
        marioPage.clickPlayButton(marioPage.getScreenshot());
    }
}
