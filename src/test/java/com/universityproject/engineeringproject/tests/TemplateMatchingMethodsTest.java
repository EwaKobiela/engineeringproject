package com.universityproject.engineeringproject.tests;

import com.universityproject.engineeringproject.helpers.ImageProcessing;
import org.junit.jupiter.api.Test;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class TemplateMatchingMethodsTest {

    private static final String templatePath = "src/test/resources/img/template";
    private static final String sourcePath = "src/test/resources/img/src";
    private static final String resultPath = "src/test/resources/img/results/";

    //expected results
    private static final double[][] expectedResult= {{543, 442}, {467, 172}, {464, 97}, {484, 49}, {644, 435},
                                                    {640, 528}, {296, 267}, {377, 351}, {465, 422}, {533, 245},
                                                    {960, 640}, {788, 340}, {450, 360}, {228, 427}, {638, 504},
                                                    {420, 220}, {340, 315}, {407, 154}, {511, 188}, {295, 388}};
    private static final double accuracy = 0.9;
    private static final int noSamples = 1;

    static ArrayList<Double[]> results = new ArrayList<Double[]>();
    static ArrayList<Double> methodAccuracy = new ArrayList<Double>();


    @Test
    public void testTemplateMatchingMethods() throws Exception {
        for(int i=0; i<20; i++){
            Double[] test = runTestUsingMethod(sourcePath+i+".jpg",
                    templatePath+i+".jpg", Imgproc.TM_SQDIFF);
            methodAccuracy.add(processResults(i));
            ImageProcessing.drawResultOnSourceImg(test,
                    sourcePath+i+".jpg",
                    templatePath+i+".jpg",
                    resultPath+"TM_CCORR/source"+i+".png");
        }
        System.out.print("Average accuracy of given method: "+calcMethodAccuracy());
    }


    //methods
    public Double[] runTestUsingMethod(String sourcePath, String templatePath, int method){
        Double[] location = new Double[1];
        results.removeAll(results);
        for(int i=0; i<noSamples; i++){
            location = ImageProcessing.findFeatureOnTheScrrenshot(sourcePath, templatePath, method);
            results.add(location);
        }
        return location;
    }

    public Double processResults(int templateNO){
        removeFalseResults(templateNO);
        return Double.valueOf(results.size()/noSamples);
    }

    public void removeFalseResults(int templateNO){
        ArrayList<Double[]> falseResults = new ArrayList<Double[]>();
        for(int i=0; i<noSamples; i++){
            double diffX = abs(expectedResult[templateNO][0]-results.get(i)[0]);
            double diffY = abs(expectedResult[templateNO][1]-results.get(i)[1]);
            if(diffX/expectedResult[templateNO][0] > 1-accuracy
                    || diffY/expectedResult[templateNO][1] > 1-accuracy)
                falseResults.add(results.get(i));
        }
        results.removeAll(falseResults);
    }

    public double calcMethodAccuracy(){
        return methodAccuracy.stream().mapToDouble(c->c).average().getAsDouble();
    }

}
